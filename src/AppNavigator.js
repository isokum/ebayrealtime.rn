import { createStackNavigator, createAppContainer } from 'react-navigation'
import MainScreen from './Main'
import WebScreen from './Web'
import ItemDetailScreen from './ItemDetail'

const AppNavigator = createStackNavigator({
    Main: MainScreen,
    Web: WebScreen,
    ItemDetail: ItemDetailScreen
})

export default createAppContainer(AppNavigator);