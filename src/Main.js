import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, SafeAreaView, StatusBar, FlatList, Image, Dimensions, TouchableHighlight } from 'react-native'
import { Icon } from 'native-base';
import ScaleImage from './widget/ScaleImage'
import CookieManager from 'react-native-cookies';

const theme = {
    colors: {
        primary: 'blue',
    },
};

export default class MainScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('headerTitle', 'eBayRealtime.RN'),
            headerLeft: <Icon name='menu' style={{ padding: 15, color: '#ffffff' }} />,
            headerStyle: {
                backgroundColor: '#1976d2',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        };
    };

    constructor(props) {
        super(props);
        this.state = { isLoading: true }
        console.disableYellowBox = true;
    }

    componentWillMount() {
        this.getHomeMain()
    }

    render() {
        return (
            <SafeAreaView style={[styles.container]}>
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.state.items}
                    renderItem={this.renderItem}
                />
            </SafeAreaView>
        );
    }


    async getHomeMain() {
        return fetch('http://gmapi.gmarket.co.kr/api/GLHome/GetHomeMainV2?charset=enUS&currency=USD&nationCode=US', {
            method: 'GET',
            headers: {
                'Authorization': 'Basic IQB0AE4AdQArAHEAcwBWAHkAWgBXAFoAbABQAHQAKwBXAGUANwBaAHYAawBjAE4AeAArAG8AUAA2AHUAYwByACsAQwBrAEsANQBFADQAOABxAFEANgBlAGcASQB2ACsAagBRAGIATgBKAGEAMwAyAE8AUwBqAFcANQBJADYAawB2AG4ATAA4ACsASABaAGoAYQBkAE4AZAArAFUAVQBpAFYATABzAEkAMwBtAFgAbwBuAFQATwBRAGYAMwBjAHkAWABuAEkAMgBWAGUARABaAGIAWABHAE4AVgBTAGwAMABxAEcALwBoAGgAUgBGAFgAWABjAHIARgBaAHkANwBiADUAWABpAEgASgBMACsAVABNADcATgBXAGQAdgAvAG4AcgBCADIAMwBDAFYAZwA9AD0A',
                'Accept': 'application/json',
                'OsType': 'A'
            }
        })
            .then((response) => {
                console.log(response)
                return response.json()
            })
            .then((responseJson) => {

                console.log(responseJson) 
                var items = []

                var banner = responseJson.Data.MainBanner[0]
                banner['cellType'] = 'banner'

                responseJson.Data.SuperDeal.forEach(item => {
                    item['cellType'] = 'deal'
                });

                items.push(banner)
                items.push.apply(items, responseJson.Data.SuperDeal)

                console.log(items)

                this.setState({
                    isLoading: false,
                    items: items,
                }, function () {
                });
            })
            .catch((error) => {
                console.error(error);
            });
    }

    keyExtractor = (item, index) => ""+index;

    renderItem = ({item}) => {
        if ( item['cellType'] === 'banner' ) 
            return this.renderBannerCell(item)
        else 
            return this.renderDealCell(item)
    }

    renderBannerCell = (item) => {
        const {navigate} = this.props.navigation
        var {height, width} = Dimensions.get('window')

        return (
            <View style={{ flex : 1 }}>
                <TouchableHighlight onPress={ () => navigate('Web', {url : item.LinkUrl, headerTitle: 'Promotion'}) }>
                    <ScaleImage width={width} uri={item.ImageUrl}/>
                </TouchableHighlight>
                <View style={{height : 10}}/>
            </View>
        )
    }

    renderDealCell = (deal) => {
        const isShowDiscountRate = deal.Item.DiscountRate.length > 0
        const {navigate} = this.props.navigation;

        return (
            <View>
                <TouchableHighlight onPress={ () => navigate('ItemDetail', { itemNo : deal.Item.GoodsCode}) }>
                    <View style={{ flex: 1, flexDirection: 'row', backgroundColor : '#ffffff'}}>
                        <Image style={{ width: 150, height: 150 }} source={{uri : deal.Item.ImageUrl}}/>
                        <View style={{ flex: 1, flexDirection: 'column', padding: 10}}>
                            <Text
                                numberOfLines={2}
                                style={{ fontSize: 15 }}>{deal.Item.GoodsName}</Text>
                            <View style={{ flexDirection: 'row', marginTop: 4}}>
                                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{deal.Item.CurrentCurrencyPrice}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 4}}>
                                <Text style={{ fontSize: 12, color: '#464646' }}>Free Shipping</Text>
                                
                                { isShowDiscountRate &&
                                    <Image style={{ width: 10, height: 10, resizeMode: 'stretch', marginTop: 6}} 
                                        source={require('./images/itemcard_tag_info_dot.png')}/>
                                }
                                
                                { isShowDiscountRate &&
                                    <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#464646' }}>
                                        {deal.Item.DiscountRate}{deal.Item.DiscountUnit} {deal.Item.DiscountSign}  
                                    </Text>
                                }
                            </View> 
                            <View style={{ flexDirection : 'row', flex : 1, marginTop: 30}} >
                                <Image style={{ width: 10, height: 10, resizeMode: 'stretch', marginTop: 3}} 
                                        source={require('./images/itemcard_star.png')}/>
                                <Text style={{ color : '#666666', fontSize: 12 }}>4.8</Text>
                            </View>
                        </View>
                    </View>
                </TouchableHighlight>
                <View style={{height : 10}}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#e6e8eb"
    }
})
