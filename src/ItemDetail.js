import React, { Component } from 'react';
import { Platform, StyleSheet, SafeAreaView, View, FlatList } from 'react-native'
import { Dimensions, TouchableHighlight, Text, Image, ToastAndroid } from 'react-native'
import { Content, Button } from 'native-base' 
import {YellowBox} from 'react-native';

import ScaleImage from './widget/ScaleImage'

export default class ItemDetailScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('headerTitle', 'Deal Item'),
            headerStyle: {
                backgroundColor: '#1976d2',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        };
    };

    constructor(props) {
        super(props)
        this.state = { isLoading: false, items: []}
        this.itemNo = this.props.navigation.getParam('itemNo')
    }

    componentWillMount() {
        console.log('componentWillMount')
        this.getExchangeRate()
        this.getDealItem()
    }

    render() {
        return (
            <SafeAreaView style={[styles.container]}>
                <View style={styles.container}>
                    <FlatList
                        data={this.state.items}
                        renderItem={this.renderListItem}/>
                    <View style={ styles.bottom}>
                        <Button full info onPress={ () => this.onAddCart() }>
                            <Text style={{ color : '#ffffff', fontWeight: 'bold'}}>Add To Cart </Text>
                        </Button> 
                    </View>
                </View>
            </SafeAreaView>
        );
    }

    getExchangeRate() {
        return fetch('http://gmapi.gmarket.co.kr/api/GLExchange/GetGlobalExchangeRate?currencyName=USD', {
                method: 'GET',
                headers: {
                    'Authorization': 'Basic IQB0AE4AdQArAHEAcwBWAHkAWgBXAFoAbABQAHQAKwBXAGUANwBaAHYAawBjAE4AeAArAG8AUAA2AHUAYwByACsAQwBrAEsANQBFADQAOABxAFEANgBlAGcASQB2ACsAagBRAGIATgBKAGEAMwAyAE8AUwBqAFcANQBJADYAawB2AG4ATAA4ACsASABaAGoAYQBkAE4AZAArAFUAVQBpAFYATABzAEkAMwBtAFgAbwBuAFQATwBRAGYAMwBjAHkAWABuAEkAMgBWAGUARABaAGIAWABHAE4AVgBTAGwAMABxAEcALwBoAGgAUgBGAFgAWABjAHIARgBaAHkANwBiADUAWABpAEgASgBMACsAVABNADcATgBXAGQAdgAvAG4AcgBCADIAMwBDAFYAZwA9AD0A',
                    'Accept': 'application/json',
                    'OsType': 'A',
                    'Content-Type': 'application/json',
                }
            })
            .then((response) => {
                console.log(response)
                return response.json()
            })
            .then((responseJson) => {
                this.exchangeRate = responseJson.Data[0]                
                console.log(this.exchangeRate) 
            })
            .catch((error) => {
                console.error(error);
            });        
    }

    getDealItem() {
        return fetch('http://gmapi.gmarket.co.kr/api/GLItem/GetGlobalGoodsInfo', {
                method: 'POST',
                body : JSON.stringify({
                    GoodsCode : this.itemNo,
                    CharacterSet : 'enUS'
                }),
                headers: {
                    'Authorization': 'Basic IQB0AE4AdQArAHEAcwBWAHkAWgBXAFoAbABQAHQAKwBXAGUANwBaAHYAawBjAE4AeAArAG8AUAA2AHUAYwByACsAQwBrAEsANQBFADQAOABxAFEANgBlAGcASQB2ACsAagBRAGIATgBKAGEAMwAyAE8AUwBqAFcANQBJADYAawB2AG4ATAA4ACsASABaAGoAYQBkAE4AZAArAFUAVQBpAFYATABzAEkAMwBtAFgAbwBuAFQATwBRAGYAMwBjAHkAWABuAEkAMgBWAGUARABaAGIAWABHAE4AVgBTAGwAMABxAEcALwBoAGgAUgBGAFgAWABjAHIARgBaAHkANwBiADUAWABpAEgASgBMACsAVABNADcATgBXAGQAdgAvAG4AcgBCADIAMwBDAFYAZwA9AD0A',
                    'Accept': 'application/json',
                    'OsType': 'A',
                    'Content-Type': 'application/json',
                }
            })
            .then((response) => {
                console.log(response)
                return response.json()
            })
            .then((responseJson) => {
                var items = []
                
                console.log(responseJson) 

                var info = responseJson.Data
                info['cellType'] = 'info'

                var review = info.GlobalData.ReviewCount
                review['cellType'] = 'review'

                var detail = info.BaseData.GoodsDetail
                detail['cellType'] = 'detail'

                items.push(info)
                items.push(review)
                items.push(detail)

                console.log(items)

                this.setState({
                    isLoading: false,
                    items: items,
                }, function () {
                });
            })
            .catch((error) => {
                console.error(error);
            });
    };

    renderListItem = ({item}) => {
        if ( item['cellType'] === 'info' ) 
            return this.renderBodyCell(item)
        else if ( item['cellType'] === 'review')
            return this.renderReviewCell(item)
        else 
            return this.renderDetailCell(item)
    };

    renderBodyCell = (item) => {
        var {height, width} = Dimensions.get('window');
        var calcPrice = Math.round((1.0 / this.exchangeRate.ExchangeRate ) * item.BaseData.Order.Price * 100) / 100
        var calcPriceText = calcPrice.toFixed(2)

        return (
            <View style={{ flex : 1 }}>
                <View style={{ backgroundColor: '#ffffff' }}>
                    <ScaleImage width={width} uri={item.BaseData.GoodsInfo.BigImageUrl}/>
                    <Text numberOfLines={2} style={{ marginLeft: 14, marginRight: 14, marginTop : 10, color: '#333333', fontSize: 16 }}>
                        {item.GlobalData.GlobalItemName}
                    </Text>
                    <View style={{ flexDirection : 'row', marginLeft : 14, marginRight : 14, marginTop: 10, marginBottom : 10}}>
                        <Text style={{ color: '#d94239', fontSize: 20, fontWeight: 'bold', marginRight: 10 }}>
                            {item.BaseData.Discount.SpanCostRatePrice}%
                        </Text>
                        <Text style={{ color: '#000000', fontSize: 20, fontWeight: 'bold' }}>
                            ${calcPriceText}
                        </Text>
                    </View>
                </View>
                <View style={{height : 10}}/>
            </View>
        )
    }

    renderReviewCell = (item) => {
        return (
            <View style={{ flex : 1 }}>
                <View style={{ flexDirection : 'row', backgroundColor: '#ffffff',  padding: 14}}>
                    <Text style={{ color: '#333333', fontSize: 16, fontWeight: 'bold' }}> Item Reviews </Text>
                    <Text style={{ color: '#333333', fontSize: 16, marginLeft: 2 }}> ({item.PhotoCount + item.TextCount}) </Text>
                </View>
                <View style={{height : 10}}/>
            </View>
        )        
    }

    renderDetailCell = (item) => {
        const {navigate} = this.props.navigation;
        const detailurl = `http://mg.gmarket.co.kr/ItemDetail?GoodsCode=${this.itemNo}`

        return (
            <View style={{ flex : 1 }}>
                <TouchableHighlight onPress={ () => navigate('Web', {url : detailurl}) }>
                    <View style={{ flexDirection : 'row', backgroundColor: '#ffffff', color: '#333333', fontSize: 16, padding: 14}}>
                        <Text style={{ color: '#333333', fontSize: 16, flex: 1 }}>
                            See Description
                        </Text>
                        <Image style={{ width: 8, height : 13, marginTop : 5}} source={require('./images/core_tier_btn_more.png')}></Image>
                    </View>
                </TouchableHighlight>
                <View style={{height : 10}}/>
            </View>
        )        
    }

    onAddCart = () => {
        ToastAndroid.showWithGravity(
            'Add to Cart Complete', 
            ToastAndroid.CENTER,
            ToastAndroid.SHORT);
    }  
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#e6e8eb"
    },
    bottom: {
        justifyContent: 'flex-end',
    }
})
