import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button } from 'react-native'
import { WebView } from 'react-native-webview';

export default class WebScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('headerTitle', 'eBayRealtime'),
            headerStyle: {
                backgroundColor: '#1976d2',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        };
    };

    constructor(props) {
        super(props);
        this.state = { isLoading: true }
        this.url = this.props.navigation.getParam('url')

        console.disableYellowBox = true;
    }

    render() {
        console.log(this.url)

        return (
            <WebView
                style={{ flex: 1}}
                source={{uri: this.url}}
                />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#f5fcff"
    },
    heading: {
        fontSize: 20,
        textAlign: "center",
        margin: 10
    }
})
