package com.ebayrealtime;

import com.facebook.react.ReactActivity;
import android.webkit.*;
import android.os.*;

public class MainActivity extends ReactActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        CookieManager.getInstance().setAcceptCookie(true);
        CookieManager.getInstance().setCookie(".gmarket.co.kr", "viewHeader=N");
        if (Build.VERSION.SDK_INT >= 21) {
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager.getInstance().sync();
        }
    }


    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "eBayRealtime";
    }


}
